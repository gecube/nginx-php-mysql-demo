FROM php:7.2-fpm-alpine

RUN apk --update add wget \
  curl \
  git \
  grep \
  build-base \
  libmemcached-dev \
  libmcrypt-dev \
  libxml2-dev \
  imagemagick-dev \
  pcre-dev \
  libtool \
  make \
  autoconf \
  g++ \
  cyrus-sasl-dev \
  libgsasl-dev \
  tzdata \
  shadow
ENV TZ Europe/Moscow

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www

COPY ./src /var/www

RUN docker-php-ext-install mysqli mbstring pdo pdo_mysql tokenizer xml opcache soap
RUN pecl channel-update pecl.php.net \
    && pecl install imagick \
    && pecl install mcrypt-1.0.1 \
    && docker-php-ext-enable imagick \
    && docker-php-ext-enable mcrypt

RUN rm /var/cache/apk/*

RUN usermod -u 1000 www-data && \
    chown -R www-data:www-data /home/www-data


EXPOSE 9000
